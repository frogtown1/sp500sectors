# US SP500 Sectors Portfolio
This project cross-compares three well-known optimization strategies (minimum variance, mean variance and equally weighted) of a portfolio constructed of the US SP500 sectors. All financial data was sourced from Yahoo Finance's API using the `quantmod` package in the R programming language whereas all analyses were conducted using the `PerformanceAnalytics` and `PortfolioAnalytics` packages.

### Usage
Simply import `00. main.R` to the latest version of the RStudio IDE and run the code top-down. All scripts used here are reproducible, meaning they can easily be repurposed for other stocks. Simply replace the stock symbols with the desired ones before running the entire script.


## Surveying Market Directions
An adjusted returns analysis is surveyed to gain a better understanding of the market's behavioral record. The timespan of the analysis is set from `Jan 2014` to `Dec 2020`:

![](images/sectors-performance-breakdown.png)

Overall, the market has been on a bullish trend that has hyper-accelerated in `Jan 2020` due to market tailwinds from increased geopolitical pressures and the COVID-19 pandemic. These tailwinds, combined with low interest rates persisted by the FED, create a perfect storm for burgeoning speculative market behavior. This, in effect, rewards investors staying long in their positions since stock prices will rise. On the otherhand, this environment can prove difficult to short sellers as entry points can be very expensive for established options, diminishing alphas. From `Jan 2014`, the sectors performed competitively amongst each other until mid-year when news of massive contractions in the oil industry (as the evident result of marked decline in oil demand from large emerging nations) catalyzed substantial downswing for the energy sector `(XLE)` that remains, to this day, seemingly irrecoverable. The other sectors, meanwhile, continued their gradual growth until about 2017 when distinct leverages surfaced where both the technology `(XLK)` and financial select `(XLF)` sectors dominated performance records throughout that year. From that point onwards till the end date of the analysis, `Dec 2020`, XLK drastically outpaced the plateauing XLF. 

The market dipped sharply going into 2020 at the breaking news of COVID-19. Interestingly this economic-wide downturn, which reached levels akin to the Great Depression, only lasted until March when market confidence was boosted by policymakers injecting trillions of dollars in stimulus.

![](images/relative-sectors-performance-breakdown.png)

A similar (albeit more dramatic) story, but it is clear that the technology sector has had the most consistent and significant upswing throughout this time period. Or in quantitative terms:

![](images/sectors-efa.png)

**Note the differences in kurtosis risk.*

## Portfolio Construction: Objectives & Constraints
With better context of the market, three portfolios will be constructed, optimized and compared: one that implements the minimum variance strategey, another mean variance, and, finally, one whose weights are distributed equally. All portfolios will be configured for risk conditions:

```
# Minimum variance
MinimumVariancePortfolio <- portfolio.spec(
  assets=colnames(secMatrix))

MinimumVariancePortfolio <- add.objective(
  portfolio=MinimumVariancePortfolio,
  type='risk',
  name='StdDev')

MinimumVariancePortfolio <- add.constraint(
  portfolio = MinimumVariancePortfolio,
  type="full_investment")

MinimumVariancePortfolio <- add.constraint(
  portfolio = MinimumVariancePortfolio,
  type="long_only")

MinimumVariancePortfolio <- add.constraint(
  portfolio = MinimumVariancePortfolio,
  type="box",
  min=0,max=0.3)

  ...*
```
**For the sake of orderliness the full code can be found in the `00. main.R` file* 

## Optimization
Each of the constructed portfolios will now be optimized so that the best asset distribution is selected with the goal of minimized financial risk(s). Here are the results of the optimization run for the minimum and mean portfolios, respectively:

![](images/opt-minvar-weights.png)
![](images/opt-meanvar-weights.png)

To visualize the risk of returns analysis:

![](images/r2r.png)

## Backtests
### Performance results

![](images/backtest-results.png)

The equally weighted portfolio finished higher than the other two. However, is this necessarily the optimal choice for investors looking for gain the most as the lowest risk?

### Annualized returns

![](images/annual-returns-with-drawdown.png)

### CAPM

![](images/portf-CAPMs.png)

No. Looking at the metrics, the minimum variance strategy would be ideal for risk-averse investors as it yields the more preferred risk-adjusted return than the other two.

![](images/portf-efas.png)

### References
#### COVID19-Economic reactions
Stimulus:
https://www.washingtonpost.com/us-policy/2020/03/24/trump-coronavirus-congress-economic-stimulus/

Downturn magnitude:
https://research.stlouisfed.org/publications/economic-synopses/2020/08/12/comparing-the-covid-19-recession-with-the-great-depression
